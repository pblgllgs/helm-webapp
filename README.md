# Create the helmchart

```bash
helm create webapp1
```

## Install the first one

```bash
helm install mywebapp-release webapp1/ --values mywebapp/values.yaml
```

## Upgrade after templating

```bash
helm upgrade mywebapp-release webapp1/ --values mywebapp/values.yaml
```

## Accessing it

```bash
minikube tunnel
```

## Create dev/prod

```bash
kubectl create namespace dev
kubectl create namespace prod
helm install mywebapp-release-dev webapp1/ --values webapp1/values.yaml -f webapp1/values-dev.yaml -n dev
helm install mywebapp-release-prod webapp1/ --values webapp1/values.yaml -f webapp1/values-prod.yaml -n prod
helm ls --all-namespaces
```

## Remove all

```bash
helm uninstall mywebapp-release-dev -n dev
helm uninstall mywebapp-release-prod -n prod
helm uninstall mywebapp-release -n default
kubectl delete namespace dev
kubectl delete namespace prod
helm ls --all-namespaces
```
